export const environment = {
  production: true,
  proyectoAngular: 'http://192.168.0.103:3000/generarProyecto',
  microservicios: 'http://192.168.0.103:4900/generar',
  modulosAngular: 'http://192.168.0.103:3300/generarModulos',
  soapClient: 'http://192.168.0.103:3800/pruebaSoap',
  descarga: 'http://192.168.0.103:3600/descarga'
};

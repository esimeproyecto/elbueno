import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TableDataSourceService {
  data;
  busquedaValida;
  dataFinal ;
  dataProyecto;
  archiValido: boolean;
  ModValido: boolean;
  dataMod;
  dataSoap;
  SoapValido: boolean;
  pathProyecto;
  microserviciosValido: boolean;
  dataMS: any;
  

  constructor(private http: HttpClient) { }

  async post(url,body){
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    await this.http.post( url, body , { headers: headers }).toPromise().then(
      res => {
        this.data = res;
        // console.log(res);
        this.busquedaValida = true;
      }, err => {
        this.busquedaValida = false;
      });
  }
  
  async get(url){
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    await this.http.get( url ,  { headers: headers }).toPromise().then(
      res => {
        this.data = res;
        this.busquedaValida = true;
      }, err => {
        this.busquedaValida = false;
      });
  }

  async mariadbNodeAngular(json){
    this.archiValido = false;
    this.dataProyecto = null;
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    await this.http.post( environment.proyectoAngular , json , { headers: headers }).toPromise().then(
      res => {
        console.log(res);
        this.dataProyecto = res;
        if (this.dataProyecto.status){
          this.archiValido = true;
          this.ModuloAngular(json, this.dataProyecto.file);
        }
      }, err => {
        console.log(err);
        this.archiValido = false;
      });
    }

    async mariadbNodeMicroservicios(json){
      this.microserviciosValido = false;
      this.dataMS = null;
      let headers = new HttpHeaders().set('Content-Type', 'application/json');
      await this.http.post( environment.microservicios, json , { headers: headers }).toPromise().then(
        res => {
          console.log(res);
          this.dataMS = res;
          this.microserviciosValido = true;
        }, err => {
          console.log(err);
          this.microserviciosValido = false;
        });
      }

    async ModuloAngular(json, path){
      this.ModValido = false;
      this.dataMod = null;
      this.pathProyecto = null;
      let headers = new HttpHeaders().set('Content-Type', 'application/json');
      await this.http.post( environment.modulosAngular, {'path': path, 'json':json} , { headers: headers }).toPromise().then(
        res => {
          console.log(res);
          this.dataMod = res;
          if (this.dataMod.status){
            this.ModValido = true;
            this.mariadbNodeMicroservicios(json);
            this.pathProyecto = path;
          }
        }, err => {
          console.log(err);
          this.ModValido = false;
        });
      }

      async SOAPService(json){
        let headers = new HttpHeaders().set('Content-Type', 'application/json');
        await this.http.post( environment.soapClient, json , { headers: headers }).toPromise().then(
          res => {
            this.data = res;
            this.busquedaValida = true;
          }, err => {
            console.log("aqui",err);
            this.busquedaValida = false;
          });
      }

      async downloadProject(data){
          return this.http.post(environment.descarga, data, {
            responseType : 'blob',
            headers:new HttpHeaders().append('Content-Type','application/json')
        });
      }


}

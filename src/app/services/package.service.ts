import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators, RequiredValidator, AbstractControl} from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class PackageService {
  forma;
  constructor() { }
  inicializaDatos(){
    this.forma = new FormGroup({
      DATABASES : new FormControl(null, Validators.required),
      FRONT : new FormControl(null, Validators.required ),
      BACK : new FormControl(null, Validators.required),
    });
  }

}

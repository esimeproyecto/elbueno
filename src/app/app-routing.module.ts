import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StepsComponent } from './components/steps/steps.component';

import { TableSelectMDBComponent } from './components/databases/table-select-mdb/table-select-mdb.component';
import { PantallaCargaComponent } from './components/pantalla-carga/pantalla-carga.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';


const routes: Routes = [
  { path: 'steps', component: StepsComponent },
  { path: 'doWork', component: PantallaCargaComponent },
  { path: 'tableSelect/mariadb', component: TableSelectMDBComponent },
  { path: 'notFound', component: PageNotFoundComponent },
  { path: '', component: StepsComponent },
  { path: '**', pathMatch:'full' , redirectTo: 'notFound' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

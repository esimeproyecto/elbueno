import { Component, OnInit } from '@angular/core';
import { PackageService } from '../../../services/package.service';

@Component({
  selector: 'app-front-options',
  templateUrl: './front-options.component.html',
  styleUrls: ['./front-options.component.css']
})
export class FrontOptionsComponent implements OnInit {

  constructor(public _packageservice: PackageService) { }

  ngOnInit(): void {
  }

  getFront(element){
    this._packageservice.forma.get('FRONT').setValue(element);
  }

}

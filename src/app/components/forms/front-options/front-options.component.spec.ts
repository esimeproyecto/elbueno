import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontOptionsComponent } from './front-options.component';

describe('FrontOptionsComponent', () => {
  let component: FrontOptionsComponent;
  let fixture: ComponentFixture<FrontOptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrontOptionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

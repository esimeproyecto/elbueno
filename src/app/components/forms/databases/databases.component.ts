import { Component, OnInit } from '@angular/core';
import { PackageService } from '../../../services/package.service';

@Component({
  selector: 'app-databases',
  templateUrl: './databases.component.html',
  styleUrls: ['./databases.component.css']
})
export class DatabasesComponent implements OnInit {

  constructor(public _packageservice: PackageService) { }

  ngOnInit(): void {
  }

  getDataBase(element){
    this._packageservice.forma.get('DATABASES').setValue(element);
  }

}

import { Component, OnInit } from '@angular/core';
import { PackageService } from '../../../services/package.service';


@Component({
  selector: 'app-middleware-options',
  templateUrl: './middleware-options.component.html',
  styleUrls: ['./middleware-options.component.css']
})
export class MiddlewareOptionsComponent implements OnInit {

  constructor(public _packageservice: PackageService) { }

  ngOnInit(): void {
  }

  getBack(element){
    this._packageservice.forma.get('BACK').setValue(element);
  }


}

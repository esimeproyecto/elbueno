import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, RequiredValidator, AbstractControl} from '@angular/forms';
import { TableDataSourceService } from '../../../services/table-data-source.service';
import {MatDialog} from '@angular/material/dialog';
import { PackageService } from '../../../services/package.service';
import { Router } from '@angular/router';
import { ModalJsonExampleComponent } from '../../modal-json-example/modal-json-example.component';
import { ModalSoapExampleComponent } from '../../modals/modal-soap-example/modal-soap-example.component';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-micro-services-options',
  templateUrl: './micro-services-options.component.html',
  styleUrls: ['./micro-services-options.component.css']
})
export class MicroServicesOptionsComponent implements OnInit {

  formaREST;
  formaSOAP;
  file: any;
  restOptions = [
  {value: 'post', viewValue: 'Post'},
  {value: 'get', viewValue: 'Get'}
];
  archivoJSON;
  data: Object;
  busquedaValida: boolean;

  constructor(private _table: TableDataSourceService, public _package: PackageService, private router: Router,
              public dialog: MatDialog, private _snackBar: MatSnackBar) {
    this.formaREST = new FormGroup({
      URL : new FormControl(null, Validators.required),
      METODO : new FormControl(null, Validators.required ),
      HEADER : new FormControl( "{'Content-Type':'application/json'}" , Validators.required),
      BODY : new FormControl('{}'),
    });

    this.formaSOAP = new FormGroup({
      URL : new FormControl(null, Validators.required),
      METODO : new FormControl(null, Validators.required ),
      ARGUMENTOS : new FormControl( ),
    });

  }

  ngOnInit(): void {
  }

  async consumeREST(){
    if (this.formaREST.get('METODO').value === 'post'){
      await this._table.post(this.formaREST.get('URL').value, this.formaREST.get('BODY').value);
      if (this._table.busquedaValida) {
        console.log('entro')
        this.router.navigate([`tableSelect/${this._package.forma.get('DATABASES').value}`]);
      } else {
        this.openSnackBar();
      }
    }
    if (this.formaREST.get('METODO').value === 'get'){
      await this._table.get(this.formaREST.get('URL').value);
      console.log(this._table.busquedaValida);
      if (this._table.busquedaValida) {
        this.router.navigate([`tableSelect/${this._package.forma.get('DATABASES').value}`]);
      } else {
        this.openSnackBar();
      }
    }
  }

  async consumeSOAP(){
    let json = {
      endPoint : this.formaSOAP.get('URL').value,
      method: this.formaSOAP.get('METODO').value,
      args: this.formaSOAP.get('ARGUMENTOS').value
    }
    console.log(json);
    await this._table.SOAPService(json);
    if (this._table.busquedaValida) {
      this.router.navigate([`tableSelect/${this._package.forma.get('DATABASES').value}`]);
    } else {
      this.openSnackBar();
    }
  }

  openDialog() {
    const dialogRef = this.dialog.open(ModalJsonExampleComponent);
    
    dialogRef.afterClosed().subscribe(result => {
    });
  }
  openDialogSoap() {
    const dialogRef = this.dialog.open(ModalSoapExampleComponent);
    
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  fileChanged(e) {
    this.file = e.target.files[0];
    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      this.archivoJSON = fileReader.result;
      this._table.data = JSON.parse(this.archivoJSON);
    }
    fileReader.readAsText(this.file);
  }

  redirectFile(){
    this.router.navigate([`tableSelect/${this._package.forma.get('DATABASES').value}`]);
  }

  openSnackBar() {
    let message;
    let style;
    message = 'Hubo un error con el servicio proporcionado!!!!';
    style = 'invalid-message';
    this._snackBar.open(message, '', {
      duration: 3000,
      horizontalPosition: 'right',
      verticalPosition: 'top',
      panelClass: [style]
    });
  }

}

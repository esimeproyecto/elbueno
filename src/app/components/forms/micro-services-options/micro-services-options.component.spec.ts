import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicroServicesOptionsComponent } from './micro-services-options.component';

describe('MicroServicesOptionsComponent', () => {
  let component: MicroServicesOptionsComponent;
  let fixture: ComponentFixture<MicroServicesOptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicroServicesOptionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicroServicesOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

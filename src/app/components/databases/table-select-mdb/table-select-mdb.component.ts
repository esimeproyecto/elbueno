import { Component, OnInit, ViewChild, ɵConsole } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {TableDataSourceService} from '../../../services/table-data-source.service';
import { FormGroup, FormControl, Validators,  ValidatorFn, FormArray} from '@angular/forms';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, DialogPosition} from '@angular/material/dialog';
import { ModalTableComponent } from '../../modals/modal-table/modal-table.component';
import { SpinnerService } from '../../../services/spinner-service.service';


@Component({
  selector: 'app-table-select-mdb',
  templateUrl: './table-select-mdb.component.html',
  styleUrls: ['./table-select-mdb.component.css']
})
export class TableSelectMDBComponent implements OnInit {
  displayedColumns: string[] = ['#', 'Tabla'];
  dataSource;
  data;
  formaArray = new FormArray([]);
  TablasSeleccionadas;
  procesoValido = true;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  busquedaValida: any;

  constructor(private _table: TableDataSourceService, public dialog: MatDialog,
              public spinnerService: SpinnerService) {
    console.log(_table.data);
    this.doWork();
  }

  async doWork(){
    try {
      this.data = await this._table.data.tables; // importa data
      this.dataSource = new MatTableDataSource(this.data); // genera dataSource para la tabla
      this.dataSource.paginator = this.paginator; // genera el paginador
      this.dataSource.filterPredicate = function(data, filter: string): boolean {
        return data.table_name.toLowerCase().includes(filter) ;
      };
      this.busquedaValida = this._table.busquedaValida;
      this.makeForms(); // genera el Groupform para cada tabla
    } catch (e){
      this.procesoValido = false;
      throw e;
    }
  }

  makeForms(){
    for (let i = 0; i < this.data.length; i ++){
      this.formaArray.push(new FormGroup({
        NOMBRE_TABLA : new FormControl(this.data[i].table_name),
        COLUMNAS_SELECCIONADAS : new FormControl(null ),
      }));
      this.formaArray.controls[i].get('COLUMNAS_SELECCIONADAS').disable();
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  change(event, i) {
    if (event.checked){
      this.formaArray.controls[i].get('COLUMNAS_SELECCIONADAS').enable();
    } else {
       this.formaArray.controls[i].get('COLUMNAS_SELECCIONADAS').disable();
    }
    console.log(event.checked);
  }

  ngOnInit(): void {
  }

  enviaDatos(){
    this.TablasSeleccionadas = new FormArray([]);
    for (let i = 0; i < this.formaArray.length; i++){
      if (this.formaArray.controls[i].get('COLUMNAS_SELECCIONADAS').enabled ){
        this.TablasSeleccionadas.push(this.formaArray.controls[i]);
      }
    }
    this._table.dataFinal = this.TablasSeleccionadas;
    console.log(this._table.dataFinal);
    this.openDialog();
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ModalTableComponent, {
      width: '500px',
      data: {valor: ''}
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed', result);
      if (result != null){
        console.log(result);
      }
    });
  }

  imprime(){
    console.log(this.TablasSeleccionadas);
    console.log('queonda',this.formaArray.controls[0].get('COLUMNAS_SELECCIONADAS').enabled);
  }

}

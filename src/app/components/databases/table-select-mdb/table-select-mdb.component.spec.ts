import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableSelectMDBComponent } from './table-select-mdb.component';

describe('TableSelectMDBComponent', () => {
  let component: TableSelectMDBComponent;
  let fixture: ComponentFixture<TableSelectMDBComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableSelectMDBComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableSelectMDBComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

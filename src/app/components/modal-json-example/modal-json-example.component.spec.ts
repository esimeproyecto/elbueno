import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalJsonExampleComponent } from './modal-json-example.component';

describe('ModalJsonExampleComponent', () => {
  let component: ModalJsonExampleComponent;
  let fixture: ComponentFixture<ModalJsonExampleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalJsonExampleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalJsonExampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

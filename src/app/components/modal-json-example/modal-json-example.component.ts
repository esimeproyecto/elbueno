import { Component, OnInit } from '@angular/core';
import { PackageService } from '../../services/package.service';

@Component({
  selector: 'app-modal-json-example',
  templateUrl: './modal-json-example.component.html',
  styleUrls: ['./modal-json-example.component.css']
})
export class ModalJsonExampleComponent implements OnInit {
  JsonMDB = `{
    'tables': [
        {
            'table_name': 'city',
            'columns': [
                {
                    'Field': 'ID',
                    'Type': 'int(11)',
                    'Null': 'NO',
                    'Key': 'PRI',
                    'Default': 'null',
                    'Extra': 'auto_increment'
                },
               ...
            ]
        }
        ...
    ]
}`;


constructor(public _package: PackageService) { }

ngOnInit(): void {
  }

}

import { Component, OnInit } from '@angular/core';
import {PackageService} from '../../services/package.service';
import {TableDataSourceService} from '../../services/table-data-source.service';
import {saveAs} from 'file-saver';
@Component({
  selector: 'app-pantalla-carga',
  templateUrl: './pantalla-carga.component.html',
  styleUrls: ['./pantalla-carga.component.css']
})
export class PantallaCargaComponent implements OnInit {
  json = [];
  constructor(public _package: PackageService, public _table: TableDataSourceService) {
    this._table.dataFinal.controls.forEach( data =>{
      this.json.push({
        table_name: data.get('NOMBRE_TABLA').value,
        columns: data.get('COLUMNAS_SELECCIONADAS').value
        }
      );
    });
    // console.log(this.json);
    this.check();
  }

  ngOnInit(): void {
  }

  check(){
    // if ( this._package.forma.get('FRONT').value === 'angular' &&
    //     this._package.forma.get('DATABASES').value === 'mariadb'
    //     && this._package.forma.get('BACK').value === 'nodejs'){
        this._table.mariadbNodeAngular(this.json);
    // }
  }

  async downloadProject(){
      let filename = 'ProyectoAngular';
      await (await this._table.downloadProject({ path: this._table.pathProyecto }))
      .subscribe(
         data => saveAs(data, filename),
          error => console.error(error)
      );
  }

  async downloadMS(){
    console.log(this._table.dataMS);
    let filename = 'Microservicios';
    await (await this._table.downloadProject({ path: this._table.dataMS.file }))
    .subscribe(
       data => saveAs(data, filename),
        error => console.error(error)
    );
}

}


import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalSoapExampleComponent } from './modal-soap-example.component';

describe('ModalSoapExampleComponent', () => {
  let component: ModalSoapExampleComponent;
  let fixture: ComponentFixture<ModalSoapExampleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalSoapExampleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalSoapExampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

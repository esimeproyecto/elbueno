import { Component, OnInit } from '@angular/core';
import { PackageService } from '../../../services/package.service';

@Component({
  selector: 'app-modal-soap-example',
  templateUrl: './modal-soap-example.component.html',
  styleUrls: ['./modal-soap-example.component.css']
})
export class ModalSoapExampleComponent implements OnInit {
  XMLMDB = `<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
  <S:Body>
     <ns2:GetTableResponse xmlns:ns2="http://service.jaxws/">
        <return>
           <columns>
              <default>null</default>
              <extra>auto_increment</extra>
              <field>ID</field>
              <key>PRI</key>
              <null>NO</null>
              <type>int(11)</type>
           </columns>
           <columns>
              <default/>
              <extra/>
              <field>Name</field>
              <key/>
              <null>NO</null>
              <type>char(35)</type>
           </columns>
           ...
           <table_name>city</table_name>
        </return>
        ...
     </ns2:GetTableResponse>
  </S:Body>
</S:Envelope>`;
  constructor(public _package: PackageService) { }

  ngOnInit(): void {
  }

}

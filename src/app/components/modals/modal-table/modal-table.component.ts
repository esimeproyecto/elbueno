import { Component, OnInit, Inject } from '@angular/core';
import { PackageService } from '../../../services/package.service';
import { TableDataSourceService } from '../../../services/table-data-source.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Router } from '@angular/router';



@Component({
  selector: 'app-modal-table',
  templateUrl: './modal-table.component.html',
  styleUrls: ['./modal-table.component.css']
})
export class ModalTableComponent implements OnInit {
  tableNames = [];

  constructor(public _package: PackageService, public _table: TableDataSourceService,
              public dialogRef: MatDialogRef<any>, @Inject(MAT_DIALOG_DATA) public data: any,
              private router: Router) {
              //  console.log(_table.dataFinal.length);
              //  console.log(_table.dataFinal.controls[0].get('NOMBRE_TABLA').value);
                for (let i = 0; i < _table.dataFinal.length; i++ ){
                  this.tableNames[i] = _table.dataFinal.controls[i].get('NOMBRE_TABLA').value;
                }
               }

  ngOnInit(): void {
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

  doWork(){
    this.onNoClick();
    this.router.navigate(['/doWork']);
  }



}

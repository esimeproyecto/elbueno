import { Component, OnInit } from '@angular/core';
import { PackageService } from '../../services/package.service';
@Component({
  selector: 'app-steps',
  templateUrl: './steps.component.html',
  styleUrls: ['./steps.component.css']
})
export class StepsComponent implements OnInit {

  constructor(public _packageservice: PackageService) {
    _packageservice.inicializaDatos();
   }

  ngOnInit(): void {
  }

  imprime(){
    console.log(this._packageservice.forma.value)
  }

}
